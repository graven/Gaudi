%Gaudi Manual
============

* @subpage guide-build-system
* @subpage code-style
* @subpage GaudiPluginService-readme
* @subpage MetaDataSvc-readme
* @subpage profiling_tools

## For Mainatiners

* @subpage release-procedure

// ============================================================================
// include files
// ============================================================================
// STL & STD
// ============================================================================
#include <algorithm>
#include <cstdlib>
#include <numeric>
// ============================================================================
/* @file GaudiCommon.cpp
 *
 *  Implementation file for class : GaudiCommon
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @author Rob Lambert Rob.Lambert@cern.ch
 *  @date   2009-08-04
 */
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgTool.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/IProperty.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ObjectVector.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/Stat.h"
#include "GaudiKernel/StatEntity.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/reverse.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiCommon.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Print.h"
// ============================================================================
// GaudiUtils
// ============================================================================
#include "GaudiUtils/RegEx.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
#include "boost/tokenizer.hpp"
// ============================================================================
// Disable warning on windows
#ifdef _WIN32
#pragma warning( disable : 4661 ) // incomplete explicit templates
#endif
// ============================================================================

// ============================================================================
// constructor initialisation
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::initGaudiCommonConstructor( const IInterface* parent )
{
  m_errorsPrint.declareUpdateHandler( &GaudiCommon<PBASE>::printErrorHandler, this );
  m_propsPrint.declareUpdateHandler( &GaudiCommon<PBASE>::printPropsHandler, this );
  m_statPrint.declareUpdateHandler( &GaudiCommon<PBASE>::printStatHandler, this );

  // setup context from parent if available
  if ( parent ) {
    if ( const GaudiAlgorithm* gAlg = dynamic_cast<const GaudiAlgorithm*>( parent ) ) {
      m_context   = gAlg->context();
      m_rootInTES = gAlg->rootInTES();
    } else if ( const GaudiTool* gTool = dynamic_cast<const GaudiTool*>( parent ) ) {
      m_context   = gTool->context();
      m_rootInTES = gTool->rootInTES();
    }
  }

  // Get the job option service
  auto jos = PBASE::template service<IJobOptionsSvc>( "JobOptionsSvc" );
  if ( !jos ) Exception( "Cannot get JobOptionsSvc" );

  // Get the "Context" option if in the file...
  const auto myList = jos->getProperties( this->name() );
  if ( myList ) {
    // Iterate over the list to set the options
    for ( const auto& iter : *myList ) {
      const Gaudi::Property<std::string>* sp = dynamic_cast<const Gaudi::Property<std::string>*>( iter );
      if ( sp ) {
        if ( iter->name().compare( "Context" ) == 0 ) {
          m_context = sp->value();
        } else if ( iter->name().compare( "RootInTES" ) == 0 ) {
          m_rootInTES = sp->value();
        }
      }
    }
  }
}
//=============================================================================

//=============================================================================
// Initialise the common functionality
//=============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::
#ifdef __ICC
    i_gcInitialize
#else
    initialize
#endif
    ()
{

  // initialize base class
  const StatusCode sc = PBASE::initialize();
  if ( sc.isFailure() ) {
    return Error( "Failed to initialise base class PBASE", sc );
  }

  // some debug printout
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "Initialize base class GaudiCommon<" << System::typeinfoName( typeid( PBASE ) ) << ">" << endmsg;
    if ( !context().empty() ) this->debug() << "Created with context = '" << context() << "'" << endmsg;
  }

  // Check rootInTES ends with a /
  if ( !m_rootInTES.empty() && m_rootInTES[m_rootInTES.size() - 1] != '/' ) m_rootInTES = m_rootInTES + "/";

  // Set up the CounterSummarySvc May need to be changed
  m_counterSummarySvc = this->svcLoc()->service( "CounterSummarySvc", false );
  if ( this->msgLevel( MSG::DEBUG ) ) {
    if ( !m_counterSummarySvc )
      this->debug() << "could not locate CounterSummarySvc, no counter summary will be made" << endmsg;
    else
      this->debug() << "found CounterSummarySvc OK" << endmsg;
  }

  // properties will be printed if asked for or in "MSG::DEBUG" mode
  if ( propsPrint() ) {
    printProps( MSG::ALWAYS );
  } else if ( this->msgLevel( MSG::DEBUG ) ) {
    printProps( MSG::DEBUG );
  }

  // update DataHandles to point to full TES location

  // get root of DataManager
  SmartIF<IDataManagerSvc> dataMgrSvc( PBASE::evtSvc() );
  auto rootName = dataMgrSvc->rootName();
  if ( !rootName.empty() && '/' != rootName.back() ) rootName += "/";

  auto fixLocation = [this, rootName]( const std::string& location ) -> std::string {
    auto tokens = boost::tokenizer<boost::char_separator<char>>{location, boost::char_separator<char>{":"}};
    auto result =
        std::accumulate( tokens.begin(), tokens.end(), std::string{}, [&]( std::string s, const std::string& tok ) {
          std::string r = fullTESLocation( tok, UseRootInTES );
          // check whether we have an absolute path if yes use it - else prepend DataManager Root
          if ( r[0] != '/' ) r = rootName + r;
          return s.empty() ? r : s + ':' + r;
        } );
    if ( result != location && this->msgLevel( MSG::DEBUG ) )
      this->debug() << "Changing " << location << " to " << result << endmsg;
    return result;
  };

  class DHHFixer : public IDataHandleVisitor
  {
  public:
    DHHFixer( std::function<std::string( const std::string& )> f ) : m_f( std::move( f ) ) {}
    void visit( const IDataHandleHolder* idhh ) override
    {
      if ( !idhh ) return;

      std::string r;
      for ( auto h : idhh->inputHandles() ) {
        r = m_f( h->objKey() );
        if ( r != h->objKey() ) h->updateKey( r );
      }
      for ( auto h : idhh->outputHandles() ) {
        r = m_f( h->objKey() );
        if ( r != h->objKey() ) h->updateKey( r );
      }
    }

  private:
    std::function<std::string( const std::string& )> m_f;
  };

  this->m_updateDataHandles = std::make_unique<DHHFixer>( fixLocation );

  return sc;
}
//=============================================================================

//=============================================================================
// Finalize the common functionality
//=============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::
#ifdef __ICC
    i_gcFinalize
#else
    finalize
#endif
    ()
{
  StatusCode sc = StatusCode::SUCCESS;

  // print the general information about statistical counters
  if ( this->msgLevel( MSG::DEBUG ) || ( statPrint() && !counters().empty() ) ) {
    // print general statistical counters
    printStat( statPrint() ? MSG::ALWAYS : MSG::DEBUG );
  }
  // add all counters to the CounterSummarySvc
  if ( m_counterSummarySvc && this->svcLoc()->existsService( "CounterSummarySvc" ) ) {
    if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "adding counters to CounterSummarySvc" << endmsg;

    Gaudi::Utils::RegEx::matchList statList{m_statEntityList.value()};
    Gaudi::Utils::RegEx::matchList counterList{m_counterList.value()};

    std::map<std::string, StatEntity> ordered_counters{begin( this->counters() ), end( this->counters() )};
    for ( const auto& i : ordered_counters ) {
      if ( statList.Or( i.first ) )
        m_counterSummarySvc->addCounter( this->name(), i.first, i.second, Gaudi::CounterSummary::SaveStatEntity );
      else if ( counterList.Or( i.first ) )
        m_counterSummarySvc->addCounter( this->name(), i.first, i.second );
    }
  }
  // release all located tools and services
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "Tools to release :";
    for ( const auto& i : m_managedTools ) {
      this->debug() << " " << i->name();
    }
    this->debug() << endmsg;
  }
  while ( !m_managedTools.empty() ) {
    sc = releaseTool( m_managedTools.back() ) && sc;
  }

  // release all located services
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "Services to release :";
    for ( const auto& i : m_services ) this->debug() << " " << i->name();
    this->debug() << endmsg;
  }
  while ( !m_services.empty() ) {
    sc = releaseSvc( m_services.front() ) && sc;
  }

  // release the CounterSummarySvc manually
  m_counterSummarySvc.reset();

  // format printout
  if ( !m_errors.empty() || !m_warnings.empty() || !m_exceptions.empty() ) {
    this->always() << "Exceptions/Errors/Warnings/Infos Statistics : " << m_exceptions.size() << "/" << m_errors.size()
                   << "/" << m_warnings.size() << "/" << m_infos.size() << endmsg;
    if ( errorsPrint() ) {
      printErrors();
    }
  }

  // clear *ALL* counters explicitly
  m_counters.clear();
  m_exceptions.clear();
  m_infos.clear();
  m_warnings.clear();
  m_errors.clear();
  m_counterList.clear();
  m_statEntityList.clear();

  // finalize base class
  return sc && PBASE::finalize();
}
//=============================================================================

//=============================================================================
// Methods related to tools and services
//=============================================================================

// ============================================================================
// manual forced (and 'safe') release of the active tool or service
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::release( const IInterface* interface ) const
{
  if ( !interface ) {
    return Error( "release(IInterface):: IInterface* points to NULL!" );
  }
  // dispatch between tools and services
  const IAlgTool* algTool = dynamic_cast<const IAlgTool*>( interface );
  // perform the actual release
  return algTool ? releaseTool( algTool ) : releaseSvc( interface );
}
// ============================================================================

// ============================================================================
// manual forced (and 'save') release of the tool
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::releaseTool( const IAlgTool* algTool ) const
{
  if ( !algTool ) {
    return Error( "releaseTool(IAlgTool):: IAlgTool* points to NULL!" );
  }
  if ( !this->toolSvc() ) {
    return Error( "releaseTool(IAlgTool):: IToolSvc* points to NULL!" );
  }
  // find a tool in the list of active tools
  auto it = std::find( m_managedTools.begin(), m_managedTools.end(), algTool );
  if ( m_managedTools.end() == it ) {
    return Warning( "releaseTool(IAlgTool):: IAlgTool* is not active" );
  }
  // get the tool
  IAlgTool* t = *it;
  // cache name
  const std::string name = t->name();
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "Releasing tool '" << name << "'" << endmsg;
  }
  // remove the tool from the lists
  PBASE::deregisterTool( t );
  m_managedTools.erase( it );
  // release tool
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "The tool '" << t->name() << "' of type '" << System::typeinfoName( typeid( *t ) )
                  << "' is released" << endmsg;
  }
  const StatusCode sc = this->toolSvc()->releaseTool( t );
  return sc.isSuccess() ? sc : Warning( "releaseTool(IAlgTool):: error from IToolSvc releasing " + name, sc );
}
// ============================================================================

// ============================================================================
// manual forced (and 'safe') release of the service
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::releaseSvc( const IInterface* Svc ) const
{
  if ( !Svc ) return Error( "releaseSvc(IInterface):: IInterface* points to NULL!" );
  SmartIF<IService> svc{const_cast<IInterface*>( Svc )};
  if ( !svc ) return Warning( "releaseSvc(IInterface):: IInterface* is not a service" );
  auto it = std::lower_bound( std::begin( m_services ), std::end( m_services ), svc, GaudiCommon_details::svc_lt );
  if ( it == m_services.end() || !GaudiCommon_details::svc_eq( *it, svc ) ) {
    return Warning( "releaseSvc(IInterface):: IInterface* is not active" );
  }
  if ( this->msgLevel( MSG::DEBUG ) ) {
    this->debug() << "Releasing service '" << ( *it )->name() << "'" << endmsg;
  }
  m_services.erase( it );
  return StatusCode::SUCCESS;
}
// ============================================================================
// ============================================================================

// ============================================================================
// Add the given service to the list of active services
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::addToServiceList( SmartIF<IService> svc ) const
{
  if ( svc ) {
    auto i = std::lower_bound( std::begin( m_services ), std::end( m_services ), svc, GaudiCommon_details::svc_lt );
    if ( i == std::end( m_services ) || !GaudiCommon_details::svc_eq( *i, svc ) ) {
      m_services.insert( i, std::move( svc ) );
    } else {
      this->warning() << "Service " << svc->name() << " already present -- skipping" << endmsg;
    }
  }
}
// ============================================================================

//=============================================================================
// Methods related to messaging
//=============================================================================

// ============================================================================
// Print the error  message and return status code
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::Error( const std::string& msg, const StatusCode st, const size_t mx ) const
{
  // increase local counter of errors
  const size_t num = ++m_errors[msg];
  // If suppressed, just return
  if ( num > mx ) {
    return st;
  } else if ( UNLIKELY( num == mx ) ) // issue one-time suppression message
  {
    return Print( "The   ERROR message is suppressed : '" + msg + "'", st, MSG::ERROR );
  }
  // return message
  return Print( msg, st, MSG::ERROR );
}
// ============================================================================

// ============================================================================
// Print the warning  message and return status code
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::Warning( const std::string& msg, const StatusCode st, const size_t mx ) const
{
  // increase local counter of warnings
  const size_t num = ++m_warnings[msg];
  // If suppressed, just return
  if ( num > mx ) {
    return st;
  } else if ( UNLIKELY( num == mx ) ) // issue one-time suppression message
  {
    return Print( "The WARNING message is suppressed : '" + msg + "'", st, MSG::WARNING );
  }
  // return message
  return Print( msg, st, MSG::WARNING );
}
// ============================================================================

// ============================================================================
// Print the info message and return status code
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::Info( const std::string& msg, const StatusCode st, const size_t mx ) const
{
  // increase local counter of warnings
  const size_t num = ++m_infos[msg];
  // If suppressed, just return
  if ( num > mx ) {
    return st;
  } else if ( UNLIKELY( num == mx ) ) // issue one-time suppression message
  {
    return Print( "The INFO message is suppressed : '" + msg + "'", st, MSG::INFO );
  }
  // return message
  return Print( msg, st, MSG::INFO );
}
// ============================================================================

// ============================================================================
//  Print the message and return status code
// ============================================================================
template <class PBASE>
StatusCode GaudiCommon<PBASE>::Print( const std::string& msg, const StatusCode st, const MSG::Level lvl ) const
{
  // perform printout ?
  if ( !this->msgLevel( lvl ) ) {
    return st;
  } // RETURN

  // use the predefined stream
  MsgStream& str = this->msgStream( lvl );
  if ( typePrint() ) {
    str << System::typeinfoName( typeid( *this ) ) << ":: ";
  }

  // print the message
  str << msg;

  // test status code
  if ( st.isSuccess() ) {
  } else if ( StatusCode::FAILURE != st ) {
    str << " StatusCode=" << st.getCode();
  } else {
    str << " StatusCode=FAILURE";
  }

  // perform print operation
  str << endmsg;

  // return
  return st;
}
// ============================================================================

// ============================================================================
// Create and (re)-throw the exception
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::Exception( const std::string& msg, const GaudiException& exc, const StatusCode sc ) const
{
  // increase local counter of exceptions
  ++m_exceptions[msg];
  Print( "Exception (re)throw: " + msg, sc, MSG::FATAL ).ignore();
  throw GaudiException( this->name() + ":: " + msg, this->name(), sc, exc );
}
// ============================================================================

// ============================================================================
// Create and (re)-throw the exception
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::Exception( const std::string& msg, const std::exception& exc, const StatusCode sc ) const
{
  // increase local counter of exceptions
  ++m_exceptions[msg];
  Print( "Exception (re)throw: " + msg, sc, MSG::FATAL ).ignore();
  throw GaudiException( this->name() + ":: " + msg + "(" + exc.what() + ")", "", sc );
}
// ============================================================================

// ============================================================================
// Create and throw the exception
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::Exception( const std::string& msg, const StatusCode sc ) const
{
  // increase local counter of exceptions
  ++m_exceptions[msg];
  Print( "Exception throw: " + msg, sc, MSG::FATAL ).ignore();
  throw GaudiException( this->name() + ":: " + msg, "", sc );
}
// ============================================================================

// ============================================================================
// perform the actual printout of counters
// ============================================================================
template <class PBASE>
long GaudiCommon<PBASE>::printStat( const MSG::Level level ) const
{
  // print statistics
  if ( counters().empty() ) {
    return 0;
  }
  MsgStream& msg = this->msgStream( level );
  //
  msg << "Number of counters : " << counters().size();
  //
  if ( !counters().empty() ) {
    msg << std::endl << m_header.value();
  }
  //
  std::map<std::string, StatEntity> ordered_counters{begin( counters() ), end( counters() )};
  for ( const auto& entry : ordered_counters ) {
    msg << std::endl
        << Gaudi::Utils::formatAsTableRow( entry.first, entry.second, m_useEffFormat, m_format1, m_format2 );
  }
  //
  msg << endmsg;
  //
  return counters().size();
}
// ============================================================================

// ============================================================================
// perform the actual printout of error counters
// ============================================================================
template <class PBASE>
long GaudiCommon<PBASE>::printErrors( const MSG::Level level ) const
{
  // format for printout
  boost::format ftm( " #%|-10s| = %|.8s| %|23t| Message = '%s'" );

  auto print = [&]( const Counter& c, const std::string& label ) {
    for ( const auto& i : c ) {
      this->msgStream( level ) << ( ftm % label % i.second % i.first ) << endmsg;
    }
  };

  print( m_exceptions, "EXCEPTIONS" );
  print( m_errors, "ERRORS" );
  print( m_warnings, "WARNINGS" );
  print( m_infos, "INFOS" );

  // return total number of errors+warnings+exceptions
  return m_exceptions.size() + m_errors.size() + m_warnings.size() + m_infos.size();
}
// ============================================================================

// ============================================================================
/** perform the printout of properties
 *  @return number of error counters
 */
// ============================================================================
template <class PBASE>
long GaudiCommon<PBASE>::printProps( const MSG::Level level ) const
{

  // print ALL properties
  MsgStream& msg         = this->msgStream( level );
  const auto& properties = this->getProperties();
  msg << "List of ALL properties of " << System::typeinfoName( typeid( *this ) ) << "/" << this->name()
      << "  #properties = " << properties.size() << endmsg;
  for ( const auto& property : reverse( properties ) ) {
    msg << "Property ['Name': Value] = " << *property << endmsg;
  }
  return properties.size();
}
// ============================================================================

// ============================================================================
// Methods for dealing with the TES and TDS
// ============================================================================

// ============================================================================
// put results into Gaudi Event Transient Store
// ============================================================================
template <class PBASE>
DataObject* GaudiCommon<PBASE>::put( IDataProviderSvc* svc, DataObject* object, const std::string& location,
                                     const bool useRootInTES ) const
{
  // check arguments
  Assert( svc, "put():: Invalid 'service'!" );
  Assert( object, "put():: Invalid 'Object'!" );
  Assert( !location.empty(), "put():: Invalid 'address' = '' " );
  // final data location
  const std::string& fullLocation = fullTESLocation( location, useRootInTES );
  // register the object!
  const StatusCode status = '/' == fullLocation[0] ? svc->registerObject( fullLocation, object )
                                                   : svc->registerObject( "/Event/" + fullLocation, object );
  // check the result!
  if ( status.isFailure() ) {
    Exception( "put():: could not register '" + System::typeinfoName( typeid( *object ) ) + "' at address '" +
                   fullLocation + "'",
               status );
  }
  if ( this->msgLevel( MSG::DEBUG ) ) {
    Print( "The object of type '" + System::typeinfoName( typeid( *object ) ) + "' is registered in TS at address '" +
               fullLocation + "'",
           status, MSG::DEBUG )
        .ignore();
  }
  return object;
}
// ============================================================================

// ============================================================================
// Handle method for changes in the 'ErrorsPrint'
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::printErrorHandler( Gaudi::Details::PropertyBase& /* theProp */ )
{
  // no action if not yet initialized
  if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) {
    return;
  }
  if ( this->errorsPrint() ) {
    this->printErrors();
  }
}
// ============================================================================
// Handle method for changes in the 'PropertiesPrint'
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::printPropsHandler( Gaudi::Details::PropertyBase& /* theProp */ )
{
  // no action if not yet initialized
  if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) {
    return;
  }
  if ( this->propsPrint() ) {
    this->printProps( MSG::ALWAYS );
  }
}
// ============================================================================
// Handle method for changes in the 'StatPrint'
// ============================================================================
template <class PBASE>
void GaudiCommon<PBASE>::printStatHandler( Gaudi::Details::PropertyBase& /* theProp */ )
{
  // no action if not yet initialized
  if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) {
    return;
  }
  if ( this->statPrint() ) {
    this->printStat( MSG::ALWAYS );
  }
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================

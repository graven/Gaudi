#ifndef GAUDIGSL_GAUDIGSLMATH_H
#define GAUDIGSL_GAUDIGSLMATH_H 1

// ============================================================================
// Include files
// ============================================================================
#include "GaudiGSL/GaudiGSL.h"
#include "GaudiGSL/GslError.h"
#include "GaudiGSL/GslErrorHandlers.h"
#include "GaudiGSL/IEqSolver.h"
#include "GaudiGSL/IFuncMinimum.h"
#include "GaudiGSL/IGslErrorHandler.h"
#include "GaudiGSL/IGslSvc.h"
// ============================================================================
#include "GaudiMath/GaudiMath.h"
// ============================================================================

#endif // GAUDIGSL_GAUDIGSLMATH_H
